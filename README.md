# pamac-aur

A Gtk3 frontend for libalpm

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/pamac/pamac-aur.git
```

**IMPORTANT NOTE:** As ***libpamac-aur*** needs to know if it is going to use flatpak or snap, it is compiled with both options active, so in case of using pamac, it will have both options active as well, which is why ***pamac-aur-snap will be eliminated***, since ***pamac-aur*** will have both options present. (Now the flatpak or snap selection is done here, not in pamac-aur).


